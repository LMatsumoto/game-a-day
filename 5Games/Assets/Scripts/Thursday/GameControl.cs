﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Update points and keep track of the game time */

	//public PlayerMovement[] pmList;		// Scripts that controls player movement

	[HideInInspector] public bool playing = false; 		// The players can move once the game starts

	public Text description;			// Description game object
	public Text countText; 				// Text game object 
	public Text timerText;				// Timer text
	public Text winText;				// Text that appears when a player reaches 700 hits
	public Text loseText;				// Text that appears if you don't make enough points
	public Button nextBtn;				// Button to go to the next game
	public Button mainBtn;				// Button to go back to the main menu 

	private float count;				// Number of points
	private float currTime;				// Current game time
	private float clockSpeed;			// Rate at which the clock will increment

	// Initalize
	void Start() {
		count = 0f;
		currTime = 101f;
		clockSpeed = 1f;

		// Start the game with the description
		StartCoroutine (StartDescription ());
	}

	IEnumerator StartDescription() {
		yield return new WaitForSeconds (8f);
		description.gameObject.SetActive (false);

		// Start the clock, we can play!
		playing = true;
		InvokeRepeating ("Clock", 0, clockSpeed);
	}
		
	// Update the amount of points
	public void UpdateCount(float c) {
		count += c;
		countText.text = "Count: " + count;
	}

	// Update the game clock
	void Clock() {
		currTime--;
		timerText.text = "Time: " + currTime;
		if (currTime == 0) {
			// If the player has enough points... they've won!
			if (count >= 700)
				winText.gameObject.SetActive (true);
			else // Or they lose :(
				loseText.gameObject.SetActive (true);

			// Let them move on to the next game if they want (or go back to the main menu)
			nextBtn.gameObject.SetActive (true);
			mainBtn.gameObject.SetActive (true);

			// End the Invoke Repeating
			CancelInvoke ();
		}
	}
}
