﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	/* GOAL OF THIS CLASS:
//	 * Control the camera movement. Follow the currently selected player while staying within the specified boundaries. */

	public GameObject player;			// Reference to currently selected UFO object
	public Transform lowLeft;			// Lower left bound for the camera
	public Transform uppRight;			// Upper right bound for the camera

	private Vector3 offset;				// Offset value of movement

	// Use this for initialization
	void Start () {
		// Get the distance between the camera position and player position
		offset = transform.position - player.transform.position;
	}

	void Update() {
		// Get the currently selected player game object
		player = GetCurrentPlayer();
	}

	// Runs every frame after all items have been processed in Update
	void LateUpdate () {
		// Align the camera with the player position within the given bounds
		float playerOffsetXPos = player.transform.position.x + offset.x;
		float playerOffsetYPos = player.transform.position.y + offset.y;

		float cameraXPos = Mathf.Clamp (playerOffsetXPos, lowLeft.transform.position.x, uppRight.transform.position.x);
		float cameraYPos = Mathf.Clamp (playerOffsetYPos, lowLeft.transform.position.y, uppRight.transform.position.y);

		transform.position = new Vector3 (cameraXPos, cameraYPos, transform.position.z);
	}

	// Return the currently selected player game object
	GameObject GetCurrentPlayer() {
		return player;
	}

	// Set the currently selected player game object
	public void SetCurrentPlayer(GameObject player) {
		this.player = player;
	}
}
