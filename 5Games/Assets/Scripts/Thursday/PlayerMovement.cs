﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Control the movement of the various players on the screen. Allow only one player to move at a time,
	 * and increment points when players hit each other. */

	public float speed;					// Player speed
	public CameraControl cs;			// Reference to Camera script
	public GameControl gc;				// Reference to Point Control script

	private Rigidbody2D rb2d;			// Reference to player's rigidbody
	private bool selected = false;		// True when game object has been selected
	// private bool playing = false;		// Determines whether or not we are playing (and can move!)

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
	}

	// Physics actions
	void FixedUpdate() {
		// The object can only move if it is selected. If so... Apply horizontal or vertical input to the object's rigidbody
		if (gc.playing && selected) {
			float h = Input.GetAxis ("Horizontal");
			float v = Input.GetAxis ("Vertical");
			Vector2 movement = new Vector2 (h, v);
			rb2d.AddForce (movement * speed);
		}
	}

	// When the player is hit... If it is hit by another player (i.e. not another wall), update the count.
	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log ("Trigger!");
		if (other.gameObject.CompareTag ("Player_")) {
			Debug.Log ("other object was Player_");
			gc.UpdateCount (1);
		}
	}

	/* public void CanMove(bool move) {
		playing = move;
		Debug.Log ("playing: " + playing);
	} */

	// When the mouse is clicked over this game object
	void OnMouseDown() {
		// If there is no currently selected player... Select this player, darken color, and assign the caera script's 'player'
		if (cs.player == null) {
			selected = true;
			this.gameObject.GetComponent<SpriteRenderer> ().color = Color.blue;
			cs.SetCurrentPlayer (this.gameObject);
			Debug.Log ("The game object has been selected");
		}
		// Otherwise, if THIS is the current player... De-select this player and set the player field to null
		else if (cs.player.Equals(this.gameObject)) {
			selected = false;
			this.gameObject.GetComponent<SpriteRenderer> ().color = Color.white;
			cs.SetCurrentPlayer (null);
			Debug.Log ("This game object has been DE-selected");
		}
	}

	// When the mouse is released
	void OnMouseUp() {
		Debug.Log ("The mouse was released.");
	}
}
