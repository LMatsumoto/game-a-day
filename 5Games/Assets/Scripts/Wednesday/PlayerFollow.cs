﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour {

	/* GOAL OF THIS CLASS: Follow the player as it bounces along the characters. */

	public SpawnPlanks sp;				// Reference to SpawnPlanks script
	public Transform player;			// Player Transform reference
	public Transform farLeft; 			// Mark leftmost side the camera can 'pan' to
	public Transform farRight; 			// Mark rightmost side for camera

	private bool endOfLevel = false; 	// Signals when it is the end of the level

	// Get current camera position, current player x-position, and make sure it is w/n the far left and far right boundaries
	void Update () {
		Vector3 newPosition = transform.position; 
		newPosition.x = player.position.x; 
		newPosition.x = Mathf.Clamp(newPosition.x, farLeft.position.x, farRight.position.x); 

		// If the camera IS at the end of the level...
		if (newPosition.x.Equals (farRight.position.x)) {
			endOfLevel = true;
			sp.SetEndOfLevel (true);
		}

		// If it is the end of the level... Keep the camera at the far right end
		if (!endOfLevel) {
			// transform.position = farRight.position;
		// } 
		// Otherwise, set the camera to the player position
		// else {
			transform.position = newPosition;
		}
	}
}
