﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonClicks : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Control what happens when you click a button at the end of the level */

	public GameObject choices;				// Buttons that appear when player is choosing between series
	public GameObject winText;				// Text that pops up when the player chooses Harry Potter

	// Wrong choice -- button turns red
	public void OnClickDark(Button b) {
		b.GetComponent<Image> ().color = Color.red;
	}

	// Wrong choice -- button turns red
	public void OnClickUnfortunate(Button b) {
		b.GetComponent<Image> ().color = Color.red;
	}

	// Correct choice! Transfer to the next level.
	public void OnClickHarry(Button b) {
		b.GetComponent<Image> ().color = Color.green; 
		StartCoroutine (CorrectChoice ()); 
	}

	// Wrong choice -- button turns red
	public void OnClickInkheart(Button b) {
		b.GetComponent<Image> ().color = Color.red;
	}

	// If the player wants to reread this level... 
	public void OnClickReread() {
		Application.LoadLevel (Application.loadedLevel);
	}

	// If the player clicks to go back to the main menu
	public void OnClickMain() {
		Application.LoadLevel (0);
	}

	// Player clicks to move on to the next level
	public void OnClickNext() {
		Application.LoadLevel (Application.loadedLevel + 1);
	}

	// Show the "You got it!" text and transfer to the next level
	IEnumerator CorrectChoice() {
		yield return new WaitForSeconds (2f);
		winText.SetActive (true);
		choices.SetActive (false);
	}
}
