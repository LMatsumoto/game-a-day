﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartLevel : MonoBehaviour {

	// Restart the current level if you fall off the letters
	void OnTriggerEnter2D(Collider2D other) {
		Application.LoadLevel(Application.loadedLevel);
	}
}
