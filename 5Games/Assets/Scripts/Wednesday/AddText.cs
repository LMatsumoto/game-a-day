﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddText : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Dynamically add text to the Canvas. The goal was to add Box Colliders to the Text, but I couldn't quite figure that out with enough time to spare.*/

	private string storyText = "It was on the corner of the street that he noticed the first sign of something peculiar -- a cat reading a map.";

	// Use this for initialization
	void Start () {
		// Initialize the GameObject for the new Text
		GameObject go = new GameObject ("Text GO");
		go.transform.SetParent (this.transform);

		// Add the Text component with necessary specifications
		Text myText = go.AddComponent<Text> ();
		myText.text = storyText;
		myText.font = Resources.GetBuiltinResource<Font> ("Arial.ttf");
		myText.fontSize = 200;
		myText.alignment = TextAnchor.LowerLeft;
		myText.color = Color.black;

		// Add the Rect Transform with necessary specifications
		Debug.Log ("RectTransform on Text");
		RectTransform rt4 = myText.GetComponent<RectTransform> ();
		rt4.anchoredPosition = new Vector2 (4459f, -102f);
		rt4.sizeDelta = new Vector2 (9585f, 296f);
		rt4.localScale = new Vector3 (1f, 1f, 1f);
	}
}
