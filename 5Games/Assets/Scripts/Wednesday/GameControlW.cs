﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControlW : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Make sure the description disappears after 4 seconds. */

	public Text description;

	// Use this for initialization
	void Start () {
		StartCoroutine (StartGame ());
	}

	// Wait for 4 seconds and then make the description disappear.
	IEnumerator StartGame() {
		yield return new WaitForSeconds (4f);
		description.gameObject.SetActive (false);
	}
}
