﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnPlanks : MonoBehaviour {

	public GameObject plankSet;			// Plank set prefab
	public GameObject[] spawnPoints;	// List of points to spawn from
	public GameObject farRight;			// Farthest right boundary the camera can move.
										// Upon reaching this point, the Choices buttons should appear.
	public GameObject choices;			// Canvas with the series choices

	private float currTime;				// Keep track of game time
	private float clockSpeed;			// Speed of game clock
	private bool endOfLevel = false;	// Signals the end of the level has been reached

	// Use this for initialization
	void Start () {
		currTime = -1f;
		clockSpeed = 1f;

		Debug.Log ("Invoking repeating");
		InvokeRepeating ("Clock", 0, clockSpeed);
	}

	public void SetEndOfLevel(bool end) {
		endOfLevel = end;
	}

	// Keep track of time past and spawn planks at random points in time
	void Clock() {
		// If we've reached the end of the level... stop spawning planks
		if (endOfLevel) {
			CancelInvoke ();
			choices.SetActive (true);
			return;
		}

		// Otherwise, continue... Increment the current amount of time
		currTime++;

		// Get a random # and if it is a multiple of the current time, spawn a wood prefab !
		float rand = Mathf.Round(Random.Range (0f, 3f));

		if (currTime % rand == 0) {
			// Get a random spawn position and instantiate the planks at that pos.
			GameObject spawnPt = spawnPoints[Random.Range(0, spawnPoints.Length)];
			Vector3 spawnPosition = (Vector3) spawnPt.transform.position;
			plankSet.gameObject.SetActive (true);
			Instantiate (plankSet, spawnPosition, Quaternion.identity);
		}
	}
}
