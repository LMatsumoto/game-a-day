﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCollision : MonoBehaviour {

	public GameObject skyParticle;		// Particle object

	// Call when object is triggered
	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log ("TriggerEnter");
		Instantiate (skyParticle, skyParticle.transform.position, skyParticle.transform.rotation);
	}
}
