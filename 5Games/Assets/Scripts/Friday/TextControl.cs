﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextControl : MonoBehaviour {

	public Text desc;		// Game description
	public Button main;		// Button to go back to the home screen
	public Button next;		// Button for moving to next game

	// Use this for initialization
	void Start () {
		StartCoroutine (StartGame ());
	}

	IEnumerator StartGame() {
		// Make sure the description disappears after 4 seconds
		yield return new WaitForSeconds (4f);
		desc.gameObject.SetActive (false);

		// After 60 seconds, allow the user to return to the main menu or move on to the next game if they want
		yield return new WaitForSeconds (60f);
		main.gameObject.SetActive (true);
		next.gameObject.SetActive (true);
	}
}
