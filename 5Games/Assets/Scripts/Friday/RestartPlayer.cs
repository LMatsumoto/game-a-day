﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartPlayer : MonoBehaviour {

	// Use when player flies off the Game screen
	void OnTriggerEnter2D(Collider2D other) {
		other.gameObject.transform.position = new Vector3 (-4f, 0f, 0f);
	}
}
