﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonControl : MonoBehaviour {

	// When the 'Next Game' button is clicked
	public void OnClickNext() {
		Application.LoadLevel (Application.loadedLevel + 1);
	}

	// When the 'Main Menu' button is clicked
	public void OnClickMain() {
		Application.LoadLevel (0);
	}
}
