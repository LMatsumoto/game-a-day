﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	/* GOAL OF THIS CLASS: Follow the player as it jumps. */

	public SpawnPlanks sp;			// Reference to SpawnPlanks script
	public Transform player;		// Player Transform reference
	public Transform top; 			// Mark upper edge the camera can 'pan' to
	public Transform bottom; 		// Mark lower edge for camera

	// Get current camera position, current player x-position, and make sure it is w/n the far left and far right boundaries
	void Update () {
		Vector3 newPosition = transform.position; 
		newPosition.y = player.position.y; 
		newPosition.y = Mathf.Clamp(newPosition.y, bottom.position.y, top.position.y); 

		transform.position = newPosition;
	}
}
