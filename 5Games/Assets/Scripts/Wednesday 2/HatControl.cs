﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatControl : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Animate the hat(s) */

	private Animator anim;			// Hat animator

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}

	// Set the speed of the hat animation
	public void SetAnimatorSpeed(float s) {
		anim.speed = s;
	}
}
