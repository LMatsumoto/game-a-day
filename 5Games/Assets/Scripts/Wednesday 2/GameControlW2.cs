﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControlW2 : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Control the # of lives the player currently has */

	[HideInInspector] public float waitTime = 4f;		// Amount of time to wait before starting animations, spawning

	public Text livesText;			// Text object that reads the # of lives the player currently has
	public Text countText;			// Text to display current # of gold coins
	public Text description;		// Text displaying description
	public Text winText;			// Text object to set active when player wins
	public Text loseText;			// Text to set active when player runs out of lives
	public Button mainBtn;			// Main menu button to set active at end of game

	public HatControl hc0;			// Bottom hat
	public HatControl hc1;			// Middle hat
	public HatControl hc2;			// Top hat

	private float lives;			// Current number of lives
	private float count;			// Current number of gold coins collected

	private SpawnCoins spawnCoins;	// Spawn Coins script

	void Start() {
		lives = 3f;
		spawnCoins = GetComponent<SpawnCoins> ();
		UpdateLivesText (lives);
		SetHatSpeeds (0f);
		StartCoroutine (StartGame ());
	}

	// After a few seconds, start the game
	IEnumerator StartGame() {
		yield return new WaitForSeconds (waitTime);
		description.gameObject.SetActive (false);
		SetHatSpeeds (1f);
	}

	// Check for the end of the game
	void Update() {
		if (lives <= 0) {
			loseText.gameObject.SetActive (true);
			EndOfGame ();
		}
		if (count >= 10) {
			winText.gameObject.SetActive (true);
			EndOfGame ();
		}
	}

	// Set the speed of the hat animators
	void SetHatSpeeds(float s) {
		hc0.SetAnimatorSpeed (s);
		hc1.SetAnimatorSpeed (s);
		hc2.SetAnimatorSpeed (s);
	}

	// Increment points whenever a gold coin is collected
	public void AddPoints() {
		count++;
		countText.text = "Count: " + count;
	}
		
	void UpdateLivesText(float l) {
		livesText.text = "Lives: " + l;
	}
		
	// At the end of the game, slow the hats, show the Main Menu button, and stop spawning coins
	void EndOfGame() {
		SetHatSpeeds (0.5f);
		mainBtn.gameObject.SetActive (true);
		spawnCoins.EndSpawn ();
	}

	// When the player falls out of the sky... Deduct a life for the box collider ONLY (or else it will count twice for the Circle Collider) and reset the player position
	void OnTriggerEnter2D(Collider2D other) {
		if (other.GetType ().Equals(typeof(BoxCollider2D))) {
			lives -= 1;
			UpdateLivesText (lives);
			other.gameObject.transform.position = new Vector3 (0f, 3.5f);
		}
	}
}
