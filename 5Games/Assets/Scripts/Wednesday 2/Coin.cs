﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Increment player points whenever a gold coin is collected */

	// Increment the current points whenever a coin is collected and destroy the coin
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag ("Player")) {
			other.GetComponent<PlayerController> ().AddPoints ();
			Destroy (this.gameObject);
		}
	}
}
