﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoins : MonoBehaviour {

	public GameObject coin; 		// Coin prefab to spawn
	public float leftBound;			// Bounds for spawning gold coins
	public float rightBound;
	public float bottomBound;
	public float topBound;

	private GameControlW2 gcw2;		// Game Control script

	// Use this for initialization
	void Start () {
		gcw2 = GetComponent<GameControlW2> ();
		InvokeRepeating ("Spawn", gcw2.waitTime, 5f);
	}

	// To end the game
	public void EndSpawn() {
		CancelInvoke ();
	}

	// Spawn the gold coins!
	void Spawn() {
		float x_ = Mathf.RoundToInt (Random.Range (leftBound, rightBound));
		float yRand = Mathf.RoundToInt (Random.Range (bottomBound, topBound));
		float y_ = (yRand % 2 != 0) ? yRand : yRand + 1; // Make sure the coin won't end up on one of the hats
		Instantiate (coin, new Vector3 (x_, y_ - 0.3f, 0f), Quaternion.identity);
	}
}
