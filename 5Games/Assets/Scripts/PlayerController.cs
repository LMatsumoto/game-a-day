﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	/* GOAL OF THIS CLASS: Control player movements. */

	[HideInInspector] public bool facingRight = true; 		// Is the player facing right?
	[HideInInspector] public bool jump = false;				// True when the space bar/jump key is pressed

	public GameControlW2 gc;								// Reference to game control script

	public float moveForce = 365f;							// Force of player movement
	public float maxSpeed = 5f;								// Maximum speed player can reach
	public float jumpForce = 1000f;							// Maximum force of jump
	public Transform groundCheck;							// Transform reference to feet (to check if player is grounded)

	private bool grounded = false;
	private Animator anim;
	private Rigidbody2D rb2d;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		rb2d = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		// Check if the player is standing on the ground
		grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
		// If so, and if the player is hitting the jump key... Jump!
		if (Input.GetButtonDown ("Jump") && grounded) {
			jump = true;
		}
	}

	void FixedUpdate() {
		float h = Input.GetAxis ("Horizontal");
		anim.SetFloat ("Speed", Mathf.Abs (h));

		// Increase player speed when moving slower than maxSpeed
		if (h * rb2d.velocity.x < maxSpeed)
			rb2d.AddForce (Vector2.right * h * moveForce);

		// Set player speed to maxSpeed when moving too quickly
		if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
			rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

		// Set the player to face the direction it is moving
		if (h > 0 && !facingRight)
			Flip ();
		else if (h < 0 && facingRight)
			Flip ();

		// Trigger jump animation, add vertical force to player, and set jump to false to prevent double jumping
		if (jump) {
			anim.SetTrigger ("Jump");
			rb2d.AddForce (new Vector2 (0f, jumpForce));
			jump = false;
		}
	}

	public void AddPoints() {
		Debug.Log ("PC Add Points");
		gc.AddPoints ();
	}

	// Flip the sprite around by scaling the x by -1
	void Flip() {
		facingRight = !facingRight;
		Vector3 newScale = transform.localScale;
		newScale.x *= -1;
		transform.localScale = newScale;
	}
}