﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Claw : MonoBehaviour {

	/* GOAL OF THIS CLASS: 
	 * Control the claw movement (rotation and raycasting) */

	public Ray rayScript;				// Script that controls ray
	public GameManager gm;				// Game Manager Script

	public GameObject ray;				// Claw that will be shooting
	public bool isShooting;				// True when shooting, cannot shoot again @ same time
	public Animator godAnimator;		// Claw animator

	// Only called once: the claw should start at a standstill
	void Start() {
		godAnimator.speed = 0f;
	}

	// Update is called once per frame
	void Update () {
		// If we are pressing the button to shoot and not currently shooting... Shoot!
		if (Input.GetButtonDown ("Fire1") && !isShooting) {
			LaunchClaw ();
		}
	}

	// This will launch the claw
	void LaunchClaw() {
		Debug.Log ("Claw is being launched...");
		// We are now shooting: No animation
		isShooting = true;
		godAnimator.speed = 0;

		Debug.Log ("Get the RayCast and Vector3");
		// Store whatever the ray hits in 'hit'
		RaycastHit hit; 
		Vector3 down = transform.TransformDirection (Vector3.up);

		Debug.Log ("If the physics Raycast has hit something...");
		// Figure out what was hit 100 units away
		if (Physics.Raycast (transform.position, down, out hit, 100)) {
			Debug.Log ("Set the ray to active!");
			ray.SetActive (true);
			Debug.Log ("Move toward the hit point");
			rayScript.RayTarget (hit.point);
		}
	}

	// Called when we collect an object
	public void CollectedObject() {
		Debug.Log ("Collected Object: speed " + godAnimator.speed.ToString ());
		// Stop shooting and re-animate the HAND_OF_GOD
		isShooting = false;
		godAnimator.speed = 1f;
	}

	// Set the speed of the claw animator to 0
	public void SetSpeed (float s) {
		godAnimator.speed = s;
	}
}
