﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ray : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Control ray movement (moving forward and retracting) and what happens when it finds a moon object. */

	public Transform origin;			// Empty game object that references the starting firing point
	public float speed = 4f;			// Speed of ray once fired
	public Claw clawScript;				// Reference to gun script
	public GameManager gm;				// Game Manager script

	private Vector3 target;				// Target where the ray is traveling to
	private GameObject child;			// When the ray finds a moon, attach it to the ray as a CHILD, so it can be moved back with the raw
	private LineRenderer lineRenderer; 	// Draw line between ray and gun
	private bool retracting;			// True once ray is traveling back to gun

	// Called on instantiation, get the line renderer of the ray
	void Awake() {
		lineRenderer = GetComponent<LineRenderer> ();
		lineRenderer.material.color = Color.green;
	}

	// Control the ray movement here
	void Update() {
		// Move from the origin point to whatever the ray hit, using the elapsed time to determine movement speed
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, target, step);
		Debug.Log ("Target: " + target.ToString ());
		lineRenderer.SetPosition (0, origin.position);
		lineRenderer.SetPosition (1, transform.position);

		// If we've reached the start while retracting... Reset the gun, deactivate the ray and start rotating again
		if (transform.position == origin.position && retracting) {
			Debug.Log ("Currently retracting");
			clawScript.CollectedObject ();
			gameObject.SetActive (false);
		}
	}

	// Set the target that the ray is moving toward
	public void RayTarget(Vector3 pos) {
		Debug.Log ("Resetting the target to "+pos.ToString());
		target = pos;
	}

	// When there is an eclipse: nullify the child game object (so the ray does not pull the moon all the way back)
	public void Eclipse() {
		Debug.Log ("Nullify the CHILD game object of the ray");
		child.transform.parent = null;
		Debug.Log ("Child: " + child.gameObject.tag);
	}

	// Way for the moon script to count the eclipse (through the Ray script)
	public void CountEclipse() {
		gm.CountEclipse (); // Count the eclipse!
	}

	// Called when the ray hits a Collider object
	void OnTriggerEnter(Collider other) {
		Debug.Log ("The ray has hit an object!");
		retracting = true;
		target = origin.position;

		// If we've caught a moon... Child the moon to the claw
		if (other.gameObject.CompareTag ("Moon")) {
			child = other.gameObject;
			other.transform.SetParent (this.transform);
		}
	}
}
