﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moon : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * When a moon is "in line" with a sun, +1 points and destroy both objects */

	public Ray rs;						// Reference to the ray script

	private Vector3 eclipsePos;			// Position of the moon during eclipse
	private GameObject sun;				// Reference to the sun game object when there is an eclipse
	private bool eclipse;				// True when there is an eclipse, false otherwise
	private double epsilon = 0.05;		// The sun and moon should be w/n this distance of each other when the eclipse occurs

	// Updates actions each frame
	void Update() {
		// If there is an eclipse... Move the moon into position and after 4 seconds destroy both game objects
		if (eclipse) {
			Debug.Log ("Eclipse update...");
			Vector3 moonPos = this.gameObject.transform.position;
			Debug.Log ("Moon pos: "+moonPos.ToString ());
			Vector3 sunPos = sun.gameObject.transform.position;
			Debug.Log ("Sun pos: " + sunPos.ToString ());
			if (Mathf.Abs (moonPos.x - sunPos.x) <= epsilon || Mathf.Abs (moonPos.y - sunPos.y) <= epsilon) {
				Debug.Log ("They are in the correct position");
				rs.Eclipse ();
				Debug.Log ("Starting CoRoutine...");
				StartCoroutine (Eclipse ());
			}
		}
	}

	// Called upon eclipse to handle object destruction
	IEnumerator Eclipse() {
		Debug.Log ("Destroy both game objects");
		yield return new WaitForSeconds (2f);
		rs.CountEclipse ();
		Debug.Log ("2 seconds over");
		Destroy (this.gameObject);
		Destroy (sun.gameObject);
	}

	// When the sun and moon collide
	void OnTriggerEnter(Collider other) {
		Debug.Log ("Something has collided with the moon!");
		if (other.gameObject.CompareTag ("Player_")) {
			Debug.Log ("It was a player!");
			rs = other.gameObject.GetComponent<Ray> ();
		}
		if (other.gameObject.CompareTag ("Sun")) {
			Debug.Log ("It was the sun!");
			eclipse = true;
			sun = other.gameObject;
		}
	}
}
