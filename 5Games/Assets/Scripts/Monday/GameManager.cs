﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Update points and keep track of the game time */

	[HideInInspector] public bool playing = false; 		// The players can move once the game starts

	public Claw cs;						// Claw script
	public Text description;			// Description game object
	public Text countText; 				// Text game object 
	public Text timerText;				// Timer text
	public Text winText;				// Text that appears when a player reaches 700 hits
	public Text loseText;				// Text that appears if you don't make enough points
	public Button nextBtn;				// Button to go to the next game
	public Button mainBtn;				// Button to go back to the main menu 
	public float winAmt;				// How much the player needs to win

	public GameObject sun;				// Sun prefab
	public GameObject moon;				// Moon prefab
	public GameObject claw;				// Claw game object
	public Transform[] sunPositions;	// List of possible sun positions

	private float count;				// Number of points
	private float currTime;				// Current game time
	private float clockSpeed;			// Rate at which the clock will increment
	private int[] sunPositionTrack;		// Private list to keep track of which sun positions have already been used 

	// Initalize
	void Start() {
		count = 0f;
		currTime = 61f;
		clockSpeed = 1f;
		sunPositionTrack = new int[sunPositions.Length];

		// Start the game with the description
		StartCoroutine (StartDescription ());
	}

	IEnumerator StartDescription() {
		yield return new WaitForSeconds (7f);
		description.gameObject.SetActive (false);

		// Start the clock, we can play!
		playing = true;
		cs.SetSpeed (1f);
		InvokeRepeating ("Clock", 0, clockSpeed);
	}
		
	// Update the amount of points
	public void CountEclipse() {
		count += 1;
		countText.text = "Total eclipses captured: " + count;
	}
		
	// Spawn eclipses on the screen
	void Spawn() {
		// Get a random sun position and build the moon position off of that 
		int posIndex = Random.Range (0, sunPositionTrack.Length);
		while (sunPositionTrack[posIndex] == -1) {
			posIndex = Random.Range (0, sunPositionTrack.Length);
		}

		Transform sunTransform = sunPositions [posIndex];
		Vector3 sunPos = new Vector3 (sunTransform.position.x, sunTransform.position.y, 0.75f);

		Vector3 moonPos = new Vector3 ();
		if (sunPos.x >= 0)
			moonPos.x = sunPos.x == 0 ? 0 : sunPos.x + 2;
		else
			moonPos.x = sunPos.x - 2;

		if (sunPos.y >= 0)
			moonPos.y = sunPos.y == 0 ? 0 : sunPos.y + 2;
		else
			moonPos.y = sunPos.y - 2; 

		// Vector3 moonPos = new Vector3 (sunPos.x == 0 ? 0 : (sunPos.x < 0 ? sunPos.x - 2 : sunPos.x + 2), sunPos.y == 0 ? 0 : (sunPos.y < 0 ? sunPos.y - 2 : sunPos.y + 2));

		// Instantiate a sun and moon game object in the scene
		Instantiate (sun, sunPos, Quaternion.identity);
		Instantiate (moon, moonPos, Quaternion.identity);

		// Remove this position from the possible list of positions (to avoid doubles)
		sunPositionTrack[posIndex] = -1;
	}

	// Control the game time
	void Clock() {
		currTime--;
		timerText.text = "Time: " + currTime;

		// Every 7 seconds while the claw is NOT shooting, spawn an eclipse!
		if (currTime % 8 == 0) {
			Spawn();
		}

		// If the game is over: Set the win or lose text and the Main Menu/Next buttons
		if (currTime == 0) {
			if (count >= winAmt)
				winText.gameObject.SetActive (true);
			else
				loseText.gameObject.SetActive (true);

			nextBtn.gameObject.SetActive (true);
			mainBtn.gameObject.SetActive (true);
			cs.SetSpeed (0f);
			claw.SetActive (false);
			CancelInvoke (); // End the invoke method
		}
	}
}
