﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuControl : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Control what happens when a user clicks a button from the main menu */

	// When the user clicks the Wednesday button
	public void OnClickWednesday() {
		Application.LoadLevel (1);
	}

	// When the user clicks the Thursday button
	public void OnClickThursday() {
		Application.LoadLevel (2);
	}

	// When the user clicks the Friday button
	public void OnClickFriday() {
		Application.LoadLevel (3);
	}

	// When the user clicks the Monday button
	public void OnClickMonday() {
		Application.LoadLevel (4);
	}

	// When the user clicks the Tuesday button
	public void OnClickTuesday() {
		Application.LoadLevel (5);
	}
}
